import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ToastController } from 'ionic-angular';
import { ClientsPage } from '../../pages/clients/clients';
import { LoginPage } from '../../pages/login/login';


/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  constructor(
    public http: HttpClient, 
    private storage: Storage,
    public toast: ToastController,
  ) {
  }
  AUTH_HEADER_KEY = 'Authorization';
  AUTH_PREFIX = 'Bearer';
  key = new Date().getTime();
  items: any = [];
  API_URL = 'https://casaebeleza.caciquedigital.com.br/api/v1/vendors/token';
  CATALOG_URL = 'https://casaebeleza.caciquedigital.com.br/api/v1/catalogs'
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Accept': 'application/json'
    })
  };

  login(mobile:string, password:string, nav: NavController ) {
    return new Promise((resolve, reject) => {
      var data = {
        mobile: mobile,
        password: password
      };
 
      this.http.post(this.API_URL, data, this.httpOptions)
        .subscribe((result: any) => {
          resolve(result.json);
          console.log(result);
          return this.storage.set('user_token', result.data).then(()=>{
            this.storage.set('vendor_id', result.vendedor.usuario.id)
            
          })
          
          .then(() => {
            nav.setRoot(ClientsPage);
            
          });
        },
        (error) => {
          reject(error.json);
          console.log(error.json);
          nav.setRoot(LoginPage);
          this.toast.create({ message: 'Usuário ou senha inválidos.', duration: 3000, position: 'botton' }).present();
        });
    });
}

}
