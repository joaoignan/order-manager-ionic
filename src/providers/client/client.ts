import { Http } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DatePipe } from '../../../node_modules/@angular/common';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { OfflineService } from '../offline/offline.service';
import { Network } from '../../../node_modules/@ionic-native/network';
import { CLIENT_API_URL } from '../../config/client-api-url.injectiontoken';

@Injectable()
export class ClientProvider extends OfflineService<Client> {

  public clients$: Observable<Client[]>;

  constructor(
    http: Http,
    storage: Storage,
    network: Network,
    @Inject(CLIENT_API_URL) clientApiUrl: string
  ) { 
    super(http, clientApiUrl, network, 'clients', storage);
    this.clients$ = this.listItems$;
  }

  // public insertClient(client: Client) {
  //   let key = `clients.${this.datepipe.transform(new Date(), "ddMMyyyyHHmmss")}`;
  //   return this.saveClient(key, client);
  // }
  

  // public updateClient(key: string, client: Client) {
  //   return this.saveClient(key, client);
  // }

  // private saveClient(key: string, client: Client) {
  //   return this.storage.set(key, client);
  // }

  // public removeClient(key: string) {
  //   return this.storage.remove(key);
  // }

  // public getAllClients() {

  //   let clients: ClientList[] = [];

  //   return this.storage.forEach((value: Client, key: string, iterationNumber: Number) => {
  //     let client = new ClientList();
  //     client.key = key;
  //     client.client = value;
  //     clients.push(client);
  //   })
  //     .then(() => {
  //       return Promise.resolve(clients);
  //     })
  //     .catch((error) => {
  //       return Promise.reject(error);
  //     });
  // }

  // getAllClients(reverse?: boolean): Promise<Client[]> {

  //   return this.storage.ready()
  //     .then((localForage: LocalForage) => {

  //       let clients: Client[] = [];

  //       return this.storage.forEach((task: Client, key: string, iterationNumber: number) => {
  //         if (key.indexOf('clients.') > -1) {
  //           clients.push(task);
  //         }
  //       }).then(() => (!reverse) ? clients : clients.reverse());

  //     });
  // }

  // get(id: number): Promise<Client> {
  //   return this.storage.get(`clients.${id}`); // tasks.46589321
  // }

  // saveClient(task: Client): Promise<Client> {
  //   return this.storage.set(`clients.${task.id}`, task);
  // }

  // updateClient(task: Client): Promise<Client> {
  //   return this.saveClient(task);
  // }

  // removeClient(id: number): Promise<boolean> {
  //   return this.storage.remove(`clients.${id}`)
  //     .then(() => true);
  // }

  get(id: number): Promise<Client> {
    return super.getFromStorage(id);
  }

  create(client: Client): Promise<Client> {
    return super.createInServer(client);
  }

  update(client: Client): Promise<Client> {
    return super.updateInServer(client);
  }

  delete(client: Client): Promise<void> {
    return super.deleteInServer(client);
  }

}

export class Client {

  public data_atual;
  public ref: Number;
  public synchronized: boolean;
  public name: string;
  public telephone: number;
  public active: boolean;
  public datepipe: DatePipe;
  public vendor_id: number;

  constructor(
  ) {
      this.ref = new Date().getTime();
      this.synchronized = false;
      this.getDate();

  }

  getDate(){
    var arraymonth = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
    var objdate = new Date
    var year = objdate.getFullYear()
    var month = objdate.getUTCMonth()
    var date = objdate.getDate()
    var hour = objdate.getHours() + ':' + objdate.getMinutes()

    this.data_atual = date +'/'+ arraymonth[month] +'/'+ year + ' - ' + hour;
  }

}

export class ClientList {
  key: string;
  client: Client;
}
