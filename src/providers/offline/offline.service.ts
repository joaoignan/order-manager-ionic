import { Headers, Http, Response } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import 'rxjs/add/operator/toPromise';

import { BaseModel } from './../../interfaces/base-model.interface';
import { Update } from './../../types/update.type';
import { Order } from '../order/order';

const AUTH_HEADER_KEY = 'Authorization';
const AUTH_PREFIX = 'Bearer';


export abstract class OfflineService<T extends BaseModel> {

    key = new Date().getTime();
    items:any = [];
    order: Order;
    protected listItems$: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([]);
    private headers: Headers = new Headers({
        'Accept': 'application/json',
        'Content-type': 'application/json'
    });
    

    private updates: Update<T>[];
    private lastUpdate: number = 0;
    private syncIntervalId: number;
    private networkOnConnectSubscription: Subscription;
    private networkOnDisconnectSubscription: Subscription;

    constructor(
        private http: Http,
        private itemApiURL: string,
        private network: Network,
        private resourceName: string, // clients //orders etc
        private storage: Storage
    ) {
        this.init();
        
    }



    private init(): void {
        console.log(this.headers);
        this.updates = [];

        this.getItemsFromCache()
            .then(() => this.getAllFromStorage())
            .then((items: T[]) => this.listItems$.next(items));
            
        this.getUpdatesFromStorage()
            .then((updates: Update<T>[]) => {
                // this.synchronize();
            }).catch((error: Error) => {
                console.log('Error sending request to server: ', error);
                return Promise.reject(error);
            });;

        this.createInterval();
        this.startNetworkListening();
        // if (this.network.type != 'unknown') {
        //     this.sync();
        // }
        

        const token = this.storage.get('user_token').then((val) => {
            if (val != null){
                this.headers.append(AUTH_HEADER_KEY, `${AUTH_PREFIX} ${val.token}`);
                // console.log(this.headers);
                // console.log(val);
                this.storage.remove('catalogos');
                this.sync();
                this.synchronize();

            }   
          });
          console.log(token);

        //   let teste = this.storage.get('catalogos')
        //   .then((val)=>{
        //         let itens: T[] = [];
        //         itens = JSON.parse(val);
                
        //     console.log(itens);
        //     this.storage.set('catalogos_json', itens);
        //   });
          
        //   this.sync();

        // localStorage.getItem('user_token');
    }

    loadVendor(){
        this.storage.get('vendor_id').then((val)=>{
          if(val != null && val != undefined){
            this.order.vendor_id = val;
            console.log(val);
          }
        });
      }

    sync() {
          this.http.get('https://casaebeleza.caciquedigital.com.br/api/v1/catalogs', {headers: this.headers})
          .map((response: Response) => response.json())
          .toPromise()
          .then((response: T[]) => {
            this.items = response;
            this.storage.set('catalogos', JSON.stringify(this.items.data))
          }).catch((error) =>{
            //   console.log(error);
          })


            // .subscribe((result) => {
            //   resolve(result.json);
            //   this.storage.remove('catalogs.');
            //   items.forEach((result) => {
            //     this.storage.set('catalogs.' + result.data.catalog_id, result);
            //   });
            //   console.log(this.headers);
              
            // },

      }

    private synchronize(): void {
        this.syncPushingToServer()
            .then((updates: Update<T>[]) => {
                this.syncPullingFromServer();
            }).catch((error: Error) => {
                console.log('Error sending request to server: ', error);
                return Promise.reject(error);
            });
    }

    private startNetworkListening(): void {

        this.networkOnConnectSubscription = this.network.onConnect()
            .subscribe(() => {
                console.info('Network connected!');
                this.createInterval();
                this.synchronize();
                 
            });

        this.networkOnDisconnectSubscription = this.network.onDisconnect()
            .subscribe(() => {
                console.info('Network disconnected!');
                this.deleteInterval();
            });

    }

    public stopNetworkListening(): void {
        this.networkOnConnectSubscription.unsubscribe();
        this.networkOnDisconnectSubscription.unsubscribe();
        this.deleteInterval();
    }

    private deleteInterval(): void {
        if (this.syncIntervalId) {
            clearInterval(this.syncIntervalId);
            this.syncIntervalId = undefined;
        }
    }

    private createInterval(): void {
        if (!this.syncIntervalId) {
            this.syncIntervalId = setInterval(() => {
                this.synchronize();
            }, 60000);
        }
    }

    private getAllFromStorage(): Promise<T[]> {
        return this.storage.ready()
            .then((localForage: LocalForage) => {

                let items: T[] = [];

                return this.storage.forEach((value: any, key: string, iterationNumber: number) => {
                    if (key.indexOf(`${this.resourceName}.`) > -1 && key.indexOf('updates.') == -1) {
                        items.push(value);
                    }
                }).then(() => {
                    return items;
                });

            });
    }

    private saveInStorage(item: T): Promise<T> {
        return this.storage.set(`${this.resourceName}.${item.ref}`, item);
    }

    private deleteFromStorage(item: T): Promise<boolean> {
        return this.storage.remove(`${this.resourceName}.${item.ref}`)
            .then(() => true);
    }

    protected getFromStorage(id: number): Promise<T> {
        return this.storage.get(`${this.resourceName}.${id}`);
    }

    private saveAllInStorage(items: T[]): Promise<T[]> {

        let promises: Promise<T>[] = [];

        items.forEach((item: T) => {
            promises.push(this.saveInStorage(item));
        });

        return Promise.all(promises);

    }

    private addUpdate(update: Update<T>): Promise<Update<T>> {
        return this.storage.set(`updates.${this.resourceName}.${update.value.ref}`, update)
            .then((up: Update<T>) => {
                this.updates.push(up);
                // sync with server
                // this.syncPushingToServer();
                return up;
            }).catch((error: Error) => {
                console.log('Error sending request to server: ', error);
                return Promise.reject(error);
            });
    }

    private removeUpdate(update: Update<T>): Promise<void> {
        return this.storage.remove(`updates.${this.resourceName}.${update.value.ref}`)
            .then(() => {
                this.updates.splice(this.updates.indexOf(update), 1);
            }).catch((error: Error) => {
                console.log('Error sending request to server: ', error);
                return Promise.reject(error);
            });
    }

    private getUpdatesFromStorage(): Promise<Update<T>[]> {
        return this.storage.ready()
            .then((localForage: LocalForage) => {

                return this.storage.forEach((value: any, key: string, iterationNumber: number) => {
                    if (key.indexOf(`updates.${this.resourceName}.`) > -1) { // updates.clients.8465445
                        this.updates.push(value);
                    }
                }).then(() => {
                    return this.updates;
                }).catch((error: Error) => {
                    console.log('Error sending request to server: ', error);
                    return Promise.reject(error);
                });;

            });
    }

    private getItemsFromCache(): Promise<void> {
        if ('caches' in window) {

            return self.caches.match(`${this.itemApiURL}${this.resourceName}`) // http://localhost:3000/api/v1/tasks (url da api em php)
                .then(response => {
                    if (response) {
                        return response 
                            .json()
                            .then(cachedJson => {
                                if (cachedJson.timestamp > this.lastUpdate) {
                                    this.listItems$.next(cachedJson.data);
                                    this.setLastUpdate(cachedJson.timestamp);
                                }
                            });
                    }
                }).catch((error: Error) => {
                    console.log('Error sending request to server: ', error);
                    return Promise.reject(error);
                });;

        } else {
            return Promise.resolve();
        }
    }

    private saveInServer(update: Update<T>): Observable<any> {

        let url: string = `${this.itemApiURL}/${this.resourceName}`; //url para acessar os métodos no servidor
        let responseObservable: Observable<Response>;

        switch(update.method) {
            case 'put':
            responseObservable = this.http[update.method](url += `/${update.value.ref}`, update.value, {headers: this.headers});
                // console.log(url);
                // console.log(this.headers);
            case 'post':
                responseObservable = this.http[update.method](url, update.value, {headers: this.headers});
                console.log(responseObservable);
                console.log(this.headers);
                break;
            case 'delete':
                url += `/${update.value.ref}`;
                responseObservable = this.http.delete(url, {headers: this.headers});
                // console.log(responseObservable);
                // console.log(this.headers);
                break;
        }

        return responseObservable
            .map((response: Response) => response.json());
        
    }

    protected createInServer(item: T): Promise<T> {

        return this.saveInStorage(item)
            .then((item: T) => {

                this.addUpdate(
                    new Update<T>('post', item)
                ).then((up: Update<T>) => {
                    let items: T[] = this.listItems$.getValue();
                    items.push(item);
                    this.listItems$.next(items);
                }).catch((error: Error) => {
                    console.log('createInServer ', error);
                    return Promise.reject(error);
                });

                return item;
            }).catch((error: Error) => {
                console.log('createInServer: ', error);
                return Promise.reject(error);
            })

    }

    protected updateInServer(item: T): Promise<T> {
        item.synchronized = false;
        return this.saveInStorage(item)
            .then((item: T) => {

                this.addUpdate(
                    new Update<T>('put', item)
                );

                return item;
            }).catch((error: Error) => {
                console.log('updateInServer: ', error);
                return Promise.reject(error);
            });
    }

    protected deleteInServer(item: T): Promise<void> {
        return this.deleteFromStorage(item)
            .then((deleted: boolean) => {

                this.addUpdate(
                    new Update<T>('delete', item)
                ).then((up: Update<T>) => {
                    let items: T[] = this.listItems$.getValue();
                    items.splice(items.indexOf(item), 1);
                    this.listItems$.next(items);
                });

            }).catch((error: Error) => {
                console.log('deleteInServer: ', error);
                return Promise.reject(error);
            });
    }

    private syncPushingToServer(): Promise<Update<T>[]> {
        let promises: Promise<Update<T>>[] = [];

        this.updates.forEach((update: Update<T>) => {

            promises.push(
                this.saveInServer(update)
                    .toPromise()
                    .then((serverData: any) => {
                        
                        this.setLastUpdate(serverData.timestamp);
                        this.removeUpdate(update);
                        if (update.method !== 'delete') {
                            this.setSynchronized(serverData.data.ref, true);
                        }

                        return update;
                    }).catch((error: Error) => {
                        console.log('syncPushingToServer: ', error);
                        return Promise.reject(error);
                    })
            );

        })

        return Promise.all(promises).catch((error: Error) => {
            console.log('syncPushingToServer: ', error);
            return Promise.reject(error);
        })
    }

    private syncPullingFromServer(): Promise<T[]> {
        return this.http.get(`${this.itemApiURL}/${this.resourceName}`, {headers: this.headers})
            .map((response: Response) => response.json())
            .toPromise()
            .then((serverData: any) => {

                if (serverData.timestamp > this.lastUpdate) {
                    this.setLastUpdate(serverData.timestamp);
                    return this.saveAllInStorage(serverData.data)
                        .then((items: T[]) => {
                            this.listItems$.next(items);
                            // clean storage
                            this.cleanStorage(items);
                            return items;
                        });
                }
                // console.log(this.headers);

                return serverData.data;
            }).catch((error: Error) => console.log('Error fetching data from server: ', error));
    }

    private cleanStorage(serverItems: T[]): void {

        if (this.lastUpdate > 0) {
            this.getAllFromStorage()
                .then((storageItems: T[]) => {
                    
                    let itemsToDelete: T[] = storageItems.filter((storageItem: T) => {
                        return !serverItems.some((serverItem: T) => serverItem.ref === storageItem.ref);
                    });

                    itemsToDelete.forEach((itemToDelete: T) => {
                        this.deleteFromStorage(itemToDelete);
                    });

                });
        }

    }

    private setSynchronized(index: number | string, synchronized: boolean): void {
        let items: T[] = this.listItems$.getValue();
        for (let i: number = 0; i < items.length; i++) {
            let item: T = items[i];
            if (item.ref === index) {
                item.synchronized = synchronized;

                this.saveInStorage(item)
                    .then(() => {
                        this.listItems$.next(items);
                    });
                
                break;
            }
        }
    }

    private setLastUpdate(timestamp: number): void {
        this.lastUpdate = timestamp;
    }
}