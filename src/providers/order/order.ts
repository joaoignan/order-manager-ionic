import { Http } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import 'rxjs/add/operator/map';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { OfflineService } from '../offline/offline.service';
import { ORDER_API_URL } from '../../config/client-api-url.injectiontoken';


@Injectable()
export class OrderProvider extends OfflineService<Order> {

  public orders$: Observable<Order[]>;
  public id:any;

  constructor(
    http: Http, 
    storage: Storage, 
    network: Network,
    @Inject(ORDER_API_URL) orderApiUrl: string
  ) {
    super(http, orderApiUrl, network, 'orders', storage);
    this.orders$ = this.listItems$;
   }

  // public insertOrder(order: Order) {
  //   let key = `orders.${this.datepipe.transform(new Date(), "ddMMyyyyHHmmss")}`;
  //   return this.saveOrder(key, order);
  // }

  // public updateOrder(key: string, order: Order) {
  //   return this.saveOrder(key, order);
  // }

  // private saveOrder(key: string, order: Order) {
  //   return this.storage.set(key, order);
  // }

  // public removeOrder(key: string) {
  //   return this.storage.remove(key);
  // }

  // public getAllOrders() {

  //   let orders: OrderList[] = [];

  //   return this.storage.forEach((value: Order, key: string, iterationNumber: Number) => {
  //     let order = new OrderList();
  //     order.key = key;
  //     order.order = value;
  //     orders.push(order);
  //   })
  //     .then(() => {
  //       return Promise.resolve(orders);
  //     })
  //     .catch((error) => {
  //       return Promise.reject(error);
  //     });
  // }

  // public getAllOrders(reverse?: boolean): Promise<OrderList[]> {

  //   return this.storage.ready()
  //     .then((localForage: LocalForage) => {

  //   let orders: OrderList[] = [];

  //       return this.storage.forEach((value: Order, key: string, iterationNumber: number) => {
  //         let order = new OrderList();
  //         order.key = key;
  //         order.order = value;
  //         if (key.indexOf('orders.') > -1) {
  //           orders.push(order);
  //         }
  //       }).then(() => (!reverse) ? orders : orders.reverse());

  //     });
  // }

  get(id: number): Promise<Order> {
    return super.getFromStorage(id);
  }

  create(order: Order): Promise<Order> {
    return super.createInServer(order);
  }

  update(order: Order): Promise<Order> {
    return super.updateInServer(order);
  }

  delete(order: Order): Promise<void> {
    return super.deleteInServer(order);
  }


}
export class Order {

  public currentdate;
  public synchronized: boolean; //
  public ref: number; //
  public catalog_id: number; 
  public name: string;
  public cod_product: string;
  public valor: string;
  public paid: boolean;
  public quantity: number;
  public client_ref: number; //
  public vendor_id: number; //

  public datepipe: DatePipe;

  constructor(
  ) {
      this.paid = false;
      this.synchronized = false;
      this.ref = new Date().getTime();
      this.getDate();
      
  }

  getDate(){
    var arraymonth = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
    var objdate = new Date
    var year = objdate.getFullYear()
    var month = objdate.getUTCMonth()
    var date = objdate.getDate()
    var hour = objdate.getHours() + ':' + objdate.getMinutes()

    this.currentdate = date +'/'+ arraymonth[month] +'/'+ year + ' - ' + hour;
  }

}

export class OrderList {
  key: string;
  order: Order;
}

export class Catalogs {
  catalog_id: number;
  name: string;
}