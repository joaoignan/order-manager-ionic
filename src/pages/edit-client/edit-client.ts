import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { ClientProvider, Client } from '../../providers/client/client'

@Component({
  selector: 'page-edit-client',
  templateUrl: 'edit-client.html',
})
export class EditClientPage {
  model: Client;
  key: string;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private clientProvider: ClientProvider,
              private toast: ToastController){
    if (this.navParams.data.client && this.navParams.data.key) {
      this.model = this.navParams.data.client;
      this.key =  this.navParams.data.key;
    } else {
      this.model = new Client();
      this.model.vendor_id = this.navParams.data.vendor_id;
    }
  }

  ionViewDidLoad(){
    console.log(this.navParams.data)
}

  save() {
    this.saveClient()
      .then(() => {
        this.toast.create({ message: 'Contato salvo.', duration: 3000, position: 'botton' }).present();
        this.navCtrl.pop();
      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao salvar o contato.', duration: 3000, position: 'botton' }).present();
      });
  }

  private saveClient() {
    if (this.model) {
      console.log(this.model);
      return this.clientProvider.create(this.model);
    } else {
      return this.clientProvider.update(this.model);
    }
  }

}
