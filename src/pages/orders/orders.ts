import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { OrderProvider, Order } from '../../providers/order/order';
import { Client } from '../../providers/client/client';
import { EditOrderPage } from '../edit-order/edit-order';

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {
  model: Client;
  orders: Order[] = [];
  filterargs = {client_ref: this.navParams.data['key']};

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private orderProvider: OrderProvider,
    private toast: ToastController){
      this.model = this.navParams.data.client;
}

  // ionViewDidEnter() {
  //   this.orderProvider.getAllOrders()
  //     .then((result) => {
  //       this.orders = result;
  //       console.log(result);
  //     });
  // }

  ionViewDidLoad() {
    this.orderProvider.orders$
    .map((orders: Order[]) => orders.slice()
    .filter(order => order.client_ref == this.navParams.data['key'])
    .reverse())
    .subscribe((orders: Order[]) => this.orders = orders);
    console.log(this.orders);
  }

  addOrder() {
    this.navCtrl.push(EditOrderPage, {keyClient: this.navParams.data['key'], vendorId: this.navParams.data['vendor']});
    console.log(this.navParams.data['key']);
    console.log(this.navParams.data['vendorId']);
  }

  editOrder(item: Order) {
    this.navCtrl.push(EditOrderPage, { key: item.ref, order: item});
  }

  removeOrder(item: Order) {
    this.orderProvider.delete(item)
      .then(() => {
        // var index = this.orders.indexOf(item);
        // this.orders.splice(index, 1);
        this.toast.create({ message: 'Pedido removido.', duration: 3000, position: 'botton' }).present();
      })
  }

}
