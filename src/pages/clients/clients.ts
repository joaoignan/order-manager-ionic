import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import { ClientProvider, Client } from '../../providers/client/client';
import 'rxjs/add/operator/map';
import { Storage } from '../../../node_modules/@ionic/storage';
import { LoginPage } from '../login/login';
import { HttpHeaders } from '@angular/common/http';
import { EditClientPage } from '../edit-client/edit-client';
import { OrdersPage } from '../orders/orders';

@Component({
  selector: 'page-clients',
  templateUrl: 'clients.html',
})


export class ClientsPage {
  // clients: ClientList[];
  clients: Client[] = [];
  vendor_id;

  constructor(public navCtrl: NavController, 
              private clientProvider: ClientProvider,
              private toast: ToastController,
              public navParams: NavParams,
              private storage: Storage,
              
            ) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Accept': 'application/json'
    })
  };

    AUTH_HEADER_KEY = 'Authorization';
    AUTH_PREFIX = 'Bearer';

  // ionViewDidEnter() {
  //   this.clientProvider.getAllClients()
  //     .then((result) => {
  //       this.clients = result;
  //       console.log(result)
  //     });

  //   this.clientProvider.getAllClients(true)
  //   .then((clients: Client[]) => {
  //     this.clients = clients;
  //   });

  //   this.clientProvider.clients$
  //   .map((clients: Client[]) => clients.slice().reverse())
  //   .subscribe((clients: Client[]) => this.clients = clients);
  // }

  ionViewDidLoad() {
    this.clientProvider.clients$
    .map((clients: Client[]) => clients.slice().reverse())
    .subscribe((clients: Client[]) => this.clients = clients);
    this.vendor_id = this.storage.get('vendor_id');
    console.log(this.vendor_id);
  }

  ionViewDidEnter() {
    this.storage.get('vendor_id').then((val)=>{
    this.vendor_id = val;
    console.log(this.vendor_id);
    })
    
  }

  addClient() {
    this.navCtrl.push(EditClientPage, {vendor_id: this.vendor_id});
    console.log(this.vendor_id);
  }

  editClient(item: Client) {
    this.navCtrl.push(EditClientPage, { key: item.ref, client: item});
    console.log(item.vendor_id);
    console.log(item.ref);
  }

  // removeClient(item: ClientList) {
  //   this.clientProvider.removeClient(item.key)
  //     .then(() => {
  //       var index = this.clients.indexOf(item);
  //       this.clients.splice(index, 1);
  //       this.toast.create({ message: 'Cliente removido.', duration: 3000, position: 'botton' }).present();
  //     })
  // }

  removeClient(item: Client) {
    this.clientProvider.delete(item)
      .then(() => {
        // var index = this.clients.indexOf(item);
        // this.clients.splice(index, 1);
        this.toast.create({ message: 'Cliente removido.', duration: 3000, position: 'botton' }).present();
      })
  }

  listOrders(itemClient: Client){
    this.navCtrl.push(OrdersPage, {key: itemClient.ref, vendor: itemClient.vendor_id, client: itemClient});
    console.log(itemClient.ref);
    console.log(itemClient.vendor_id);
  }

  logout(){
    this.storage.remove('user_token');
    this.navCtrl.setRoot(LoginPage);
  }

}
