import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup } from '../../../node_modules/@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  form: FormGroup;

  constructor(
    public navCtrl: NavController, 
    private auth: AuthProvider,
  ) {}

  public mobile: string;
  public password: string;

  loginIn() {
    this.auth.login(this.mobile, this.password, this.navCtrl).then(succ => console.log(succ)).catch(err => console.log(err));
}


}
