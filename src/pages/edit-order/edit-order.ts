import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { Order, OrderProvider } from '../../providers/order/order';
import { Storage } from '../../../node_modules/@ionic/storage';

@Component({
  selector: 'page-edit-order',
  templateUrl: 'edit-order.html',
})
export class EditOrderPage {
  model: Order;
  key: string;
  catalogs:any = [];
  vendor_id:number;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private orderProvider: OrderProvider,
    private storage: Storage,
    private toast: ToastController){
      
if (this.navParams.data.order && this.navParams.data.key) {
this.model = this.navParams.data.order;
this.key =  this.navParams.data.key;
} else {
  this.model = new Order();
  this.model.client_ref = this.navParams.data.keyClient;
  this.model.vendor_id = this.navParams.data.vendorId;
}
}

ionViewDidLoad(){
  console.log(this.navParams.data)
  this.loadCatalogs();
  // this.loadVendor();
  // console.log(this.vendor_id);


}

ionViewDidEnter(){
  console.log(this.navParams.data)

}

loadCatalogs(){
  this.storage.get('catalogos').then((val)=>{
    if(val != null && val != undefined){
      this.catalogs = JSON.parse(val);
    }
  })
}

catalogChange(){
  console.log(this.catalogs);
  this.catalogs.map(catalog => {
    if(catalog.catalog_id == this.model.catalog_id){
      this.model.name = catalog.name;
    }
    console.log(catalog.name);
  });
}

loadVendor(){
  this.storage.get('vendor_id').then((val)=>{
    if(val != null && val != undefined){
      this.vendor_id = val;
      console.log(this.vendor_id);
    }
  });
}

saveOrders() {
  this.saveOrder()
    .then(() => {
      this.toast.create({ message: 'Pedido salvo.', duration: 3000, position: 'botton' }).present();
      this.navCtrl.pop();
    })
    .catch(() => {
      this.toast.create({ message: 'Erro ao salvar o pedido.', duration: 3000, position: 'botton' }).present();
    });
}

private saveOrder() {
  if (this.key) {
    console.log(this.key);
    return this.orderProvider.update(this.model);
  } else {
    return this.orderProvider.create(this.model);
  }
}

}
