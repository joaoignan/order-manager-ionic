import { InjectionToken } from "@angular/core";

export const CLIENT_API_URL = new InjectionToken<string>('CLIENT_API_URL');
export const ORDER_API_URL = new InjectionToken<string>('ORDER_API_URL');
export const TOKEN_API_URL = new InjectionToken<string>('TOKEN_API_URL');