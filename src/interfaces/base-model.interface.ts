export interface BaseModel {

    ref: Number;
    synchronized: boolean;

}