import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from "@angular/http";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ClientProvider } from '../providers/client/client';
import { OrderProvider } from '../providers/order/order';
import { ClientsPage } from '../pages/clients/clients';
import { IonicStorageModule } from '../../node_modules/@ionic/storage';
import { DatePipe } from '@angular/common';
import { LoginPage } from '../pages/login/login';
import { CLIENT_API_URL, ORDER_API_URL } from '../config/client-api-url.injectiontoken';
import { Network } from '../../node_modules/@ionic-native/network';
import { AuthProvider } from '../providers/auth/auth';
import { HttpClientModule } from '@angular/common/http';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { OrdersPage } from '../pages/orders/orders';
import { EditClientPage } from '../pages/edit-client/edit-client';
import { EditOrderPage } from '../pages/edit-order/edit-order';
// import { Interceptor } from './auth/interceptor.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ClientsPage,
    LoginPage,
    OrdersPage,
    EditClientPage,
    EditOrderPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    BrMaskerModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ClientsPage,
    LoginPage,
    OrdersPage,
    EditClientPage,
    EditOrderPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatePipe,
    ClientProvider,
    Network,
    OrderProvider,
    { provide: CLIENT_API_URL, useValue: 'https://casaebeleza.caciquedigital.com.br/api/v1' }, //mudar para a URL do cakephp
    { provide: ORDER_API_URL, useValue: 'https://casaebeleza.caciquedigital.com.br/api/v1' },
    AuthProvider

  ]
})
export class AppModule {}
