import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../pages/login/login';
import { ClientsPage } from '../pages/clients/clients';
import { HttpHeaders } from '@angular/common/http';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

   

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    private storage: Storage) {
    this.initializeApp();
  }
  key = new Date().getTime();
  items:any = [];
  AUTH_HEADER_KEY = 'Authorization';
  AUTH_PREFIX = 'Bearer';
  API_URL = 'https://casaebeleza.caciquedigital.com.br/api/v1/vendors/token';
  httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Accept': 'application/json'
  })
};
  
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // this.storage.get('user_token').then((val) =>{
      //   this.httpOptions.headers.append(this.AUTH_HEADER_KEY, `${this.AUTH_PREFIX} ${val.token}`)
      // });

    });

    let token = this.storage.get('user_token').then((val) => {
      if (val != null){
        this.rootPage = ClientsPage;
        // console.log(val);
        console.log(token);
      }else{
        this.rootPage = LoginPage;
      }
    });

    // if (this.network.type === 'wifi') {
    //   this.sync();
    // }

  }

  // sync() {
  //   return new Promise((resolve, reject) => {
  //     this.storage.get('user_token').then((val) => {
  //       if (val != null){
  //           this.httpOptions.headers.append(this.AUTH_HEADER_KEY, `${this.AUTH_PREFIX} ${val.token}`);
  //           console.log(val);
  //       }
  //     }).then(()=>
  //   {
  //     this.http.get('https://casaebeleza.caciquedigital.com.br/api/v1/catalogs', {headers: this.httpOptions.headers})
  //       .subscribe((result) => {
  //         resolve(result);
  //         this.storage.remove('catalogs.');
  //           localStorage.setItem('catalogs.', JSON.stringify(result));
          
  //       },
  //       (error) => {
  //         reject(error.json);
  //         console.log(error.json);
  //       });
  //   });

  //   });
  // }

//   sync() {
//     this.http.get('https://casaebeleza.caciquedigital.com.br/api/v1/catalogs', {headers: this.httpOptions.headers})
//     .map((response: Response) => response.json())
//     .toPromise()
//     .then((response: any) => {
//       this.items = response;
//       this.storage.set('catalogs', JSON.stringify(this.items.data))
//     }).catch((error) =>{
//         console.log(error);
//     })


//       // .subscribe((result) => {
//       //   resolve(result.json);
//       //   this.storage.remove('catalogs.');
//       //   items.forEach((result) => {
//       //     this.storage.set('catalogs.' + result.data.catalog_id, result);
//       //   });
//       //   console.log(this.headers);
        
//       // },

// }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

}
