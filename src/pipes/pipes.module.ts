import { NgModule } from '@angular/core';
import { ClientrefPipe } from './clientref/clientref';
@NgModule({
	declarations: [ClientrefPipe],
	imports: [],
	exports: [ClientrefPipe]
})
export class PipesModule {}
