import { Pipe, PipeTransform } from '@angular/core';
import { Order } from '../../providers/order/order';

/**
 * Generated class for the ClientrefPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'clientref',
  pure: false
})
export class ClientrefPipe implements PipeTransform {
  transform(items: any[], filter: Order): any {  
    if (!items || !filter) {  
        return items;  
    }  
    return items.filter(item => item.client_ref.indexOf(filter.client_ref) !== -1);  
}  
}
